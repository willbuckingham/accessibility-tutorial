"use strict";
/*! rc_header_v1.js */
var rc = {};

"use strict";
/*! dc_header_v1.js */
var dc = {};

"use strict";
/*! accessibilityTree/accessibilityTreePageComponent.jsx */
rc.accessibilityTreePageComponent = React.createClass({
				displayName: "accessibilityTreePageComponent",
				render: function render() {
								console.log(this.constructor.displayName + ' render()');
								var output;
								return React.createElement(
												"div",
												{ id: "accessibilityTree", className: "container-fluid" },
												React.createElement(
																"header",
																null,
																React.createElement(
																				"h2",
																				null,
																				"Accessiblity Tree"
																)
												),
												React.createElement(
																"nav",
																null,
																React.createElement(
																				"a",
																				{ href: "#BADLINK" },
																				"BAD LINK NAV ITEM"
																)
												),
												React.createElement(
																"section",
																null,
																React.createElement(
																				"article",
																				null,
																				"Heres some content"
																)
												),
												React.createElement(
																"footer",
																null,
																"Bottom"
												)
								);
				}
});
"use strict";
/*! accessibleButtonEvents/accessibleButtonEventsPageComponent.jsx */
rc.accessibleButtonEventsPageComponent = React.createClass({
    displayName: "accessibleButtonEventsPageComponent",
    clickHandler: function clickHandler() {
        alert("hi");
    },
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        var output;
        switch (app.status.currentFragsArray[0]) {
            case "bad":
                output = React.createElement(
                    "div",
                    { className: "btn" },
                    React.createElement("i", { className: "fa fa-bars" })
                );
                break;
            case "good":
                output = React.createElement(
                    "div",
                    { className: "btn", role: "button", tabIndex: "0", "aria-label": "Menu", onClick: this.clickHandler, onKeyPress: this.clickHandler },
                    React.createElement("i", { className: "fa fa-bars" })
                );
                break;
        }
        return React.createElement(
            "div",
            { id: "accessibleButtonEvents", className: "container-fluid" },
            React.createElement(
                "h2",
                null,
                "Accessible Button Events"
            ),
            React.createElement(
                "button",
                { className: "btn", "aria-label": "Help", onClick: this.clickHandler },
                React.createElement("i", { className: "fa fa-question-circle" })
            ),
            output
        );
    }
});
"use strict";
/*! accessibleIconButtons/accesibleIconButtonsPageComponent.jsx */
rc.accesibleIconButtonsPageComponent = React.createClass({
	displayName: "accesibleIconButtonsPageComponent",
	render: function render() {
		console.log(this.constructor.displayName + ' render()');
		var output;
		if (app.status.currentFragsArray[0] == "bad") {
			output = React.createElement(
				"div",
				null,
				React.createElement(
					"button",
					{ className: "btn" },
					"Help"
				),
				React.createElement(
					"button",
					{ className: "btn" },
					React.createElement("i", { className: "fa fa-question-circle" })
				),
				React.createElement(
					"div",
					{ className: "btn" },
					React.createElement(
						"svg",
						{ width: "32", height: "32", viewBox: "0 0 32 32", className: "icon" },
						React.createElement("path", { d: "M14 24h4v-4h-4v4zM16 8c-3 0-6 3-6 6h4c0-1 1-2 2-2s2 1 2 2c0 2-4 2-4 4h4c2-0.688 4-2 5-5s-3-5-6-5zM16 0c-8.844 0-16 7.156-16 16s7.156 16 16 16 16-7.156 16-16-7.156-16-16-16zM16 28c-6.625 0-12-5.375-12-12s5.375-12 12-12 12 5.375 12 12-5.375 12-12 12z" })
					)
				)
			);
		} else if (app.status.currentFragsArray[0] == "good") {
			output = React.createElement(
				"div",
				null,
				React.createElement(
					"button",
					{ className: "btn" },
					"Help"
				),
				React.createElement(
					"button",
					{ className: "btn" },
					React.createElement(
						"span",
						{ className: "sr-only" },
						"Help"
					),
					React.createElement("i", { className: "fa fa-question-circle", "aria-hidden": "true" })
				),
				React.createElement(
					"div",
					{ className: "btn", role: "button", tabindex: "0" },
					React.createElement(
						"svg",
						{ width: "32", height: "32", viewBox: "0 0 32 32", className: "icon", "aria-labelledby": "title" },
						React.createElement(
							"title",
							{ id: "title" },
							"Help"
						),
						React.createElement("path", { d: "M14 24h4v-4h-4v4zM16 8c-3 0-6 3-6 6h4c0-1 1-2 2-2s2 1 2 2c0 2-4 2-4 4h4c2-0.688 4-2 5-5s-3-5-6-5zM16 0c-8.844 0-16 7.156-16 16s7.156 16 16 16 16-7.156 16-16-7.156-16-16-16zM16 28c-6.625 0-12-5.375-12-12s5.375-12 12-12 12 5.375 12 12-5.375 12-12 12z" })
					)
				)
			);
		}
		return React.createElement(
			"div",
			{ id: "accessibleIconButtons", className: "container-fluid" },
			React.createElement(
				"h2",
				null,
				"Accessible Icon Buttons"
			),
			output
		);
	}
});
'use strict';
/*! focusManagement/focusManagementPageComponent.jsx */
rc.focusManagementPageComponent = React.createClass({
    displayName: 'focusManagementPageComponent',
    clickHandlerBad: function clickHandlerBad(evt) {
        jQuery(evt.target).parent().remove();
    },
    clickHandlerGood: function clickHandlerGood(evt) {
        jQuery(evt.target).parent().remove();
        jQuery("li").find('.btn-delete').first().focus();
    },
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        var output;
        switch (app.status.currentFragsArray[0]) {
            case "bad":
                output = React.createElement(
                    'ul',
                    { className: 'list' },
                    React.createElement(
                        'li',
                        null,
                        'Item 1 ',
                        React.createElement(
                            'button',
                            { className: 'btn-delete', 'aria-label': 'Delete Item 1', onClick: this.clickHandlerBad },
                            ' X '
                        )
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Item 2 ',
                        React.createElement(
                            'button',
                            { className: 'btn-delete', 'aria-label': 'Delete Item 2', onClick: this.clickHandlerBad },
                            ' X '
                        )
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Item 3 ',
                        React.createElement(
                            'button',
                            { className: 'btn-delete', 'aria-label': 'Delete Item 3', onClick: this.clickHandlerBad },
                            ' X '
                        )
                    )
                );
                break;
            case "good":
                output = React.createElement(
                    'ul',
                    { className: 'list' },
                    React.createElement(
                        'li',
                        null,
                        'Item 1 ',
                        React.createElement(
                            'button',
                            { className: 'btn-delete', 'aria-label': 'Delete Item 1', onClick: this.clickHandlerGood },
                            ' X '
                        )
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Item 2 ',
                        React.createElement(
                            'button',
                            { className: 'btn-delete', 'aria-label': 'Delete Item 2', onClick: this.clickHandlerGood },
                            ' X '
                        )
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Item 3 ',
                        React.createElement(
                            'button',
                            { className: 'btn-delete', 'aria-label': 'Delete Item 3', onClick: this.clickHandlerGood },
                            ' X '
                        )
                    )
                );
                break;
        }
        return React.createElement(
            'div',
            { id: 'accessibleButtonEvents', className: 'container-fluid' },
            React.createElement(
                'h2',
                null,
                'Focus Management'
            ),
            React.createElement(
                'p',
                null,
                React.createElement(
                    'a',
                    { href: '#' },
                    'A link'
                ),
                '.'
            ),
            output
        );
    }
});
"use strict";
/*! accessibleForms/accessibleFormsPageComponent.jsx */
rc.accessibleFormsPageComponent = React.createClass({
    displayName: "accessibleFormsPageComponent",
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        var output;
        switch (app.status.currentFragsArray[0]) {
            case "bad":
                output = React.createElement(
                    "div",
                    null,
                    "Your Name",
                    React.createElement("input", { type: "text" }),
                    React.createElement(
                        "p",
                        null,
                        "Favorite Animal"
                    ),
                    React.createElement(
                        "div",
                        null,
                        React.createElement("input", { type: "radio", name: "animals" }),
                        "Dog"
                    ),
                    React.createElement(
                        "div",
                        null,
                        React.createElement("input", { type: "radio", name: "animals" }),
                        "Cat"
                    ),
                    React.createElement(
                        "div",
                        null,
                        React.createElement("input", { type: "radio", name: "animals" }),
                        "Bear"
                    ),
                    React.createElement("br", null),
                    React.createElement(
                        "div",
                        { className: "label-wrap" },
                        "Hometown"
                    ),
                    React.createElement(
                        "div",
                        { className: "input-wrap" },
                        React.createElement("input", { type: "text" })
                    )
                );
                break;
            case "good":
                output = React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "label",
                        null,
                        "Your Name",
                        React.createElement("input", { type: "text" })
                    ),
                    React.createElement(
                        "p",
                        null,
                        "Favorite Animal"
                    ),
                    React.createElement(
                        "div",
                        null,
                        React.createElement(
                            "label",
                            null,
                            React.createElement("input", { type: "radio", name: "animals" }),
                            "Dog"
                        )
                    ),
                    React.createElement(
                        "div",
                        null,
                        React.createElement(
                            "label",
                            null,
                            React.createElement("input", { type: "radio", name: "animals" }),
                            "Cat"
                        )
                    ),
                    React.createElement(
                        "div",
                        null,
                        React.createElement(
                            "label",
                            null,
                            React.createElement("input", { type: "radio", name: "animals" }),
                            "Bear"
                        )
                    ),
                    React.createElement("br", null),
                    React.createElement(
                        "div",
                        { className: "label-wrap" },
                        React.createElement(
                            "label",
                            { htmlFor: "hometown" },
                            "Hometown"
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "input-wrap" },
                        React.createElement("input", { type: "text", id: "hometown" })
                    )
                );
                break;
            case "best":
                output = React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "label",
                        null,
                        "Your Name",
                        React.createElement("input", { type: "text" })
                    ),
                    React.createElement("br", null),
                    React.createElement("br", null),
                    React.createElement(
                        "fieldset",
                        null,
                        React.createElement(
                            "legend",
                            null,
                            "Favorite Animal"
                        ),
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "label",
                                null,
                                React.createElement("input", { type: "radio", name: "animals" }),
                                "Dog"
                            )
                        ),
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "label",
                                null,
                                React.createElement("input", { type: "radio", name: "animals" }),
                                "Cat"
                            )
                        ),
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "label",
                                null,
                                React.createElement("input", { type: "radio", name: "animals" }),
                                "Bear"
                            )
                        )
                    ),
                    React.createElement("br", null),
                    React.createElement(
                        "div",
                        { className: "label-wrap" },
                        React.createElement(
                            "label",
                            { "for": "hometown" },
                            "Hometown"
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "input-wrap" },
                        React.createElement("input", { type: "text", id: "hometown" })
                    )
                );
                break;
        }
        return React.createElement(
            "div",
            { id: "accessibleForms", className: "container-fluid" },
            React.createElement(
                "h2",
                null,
                "Building Forms with Accessibility in Mind"
            ),
            React.createElement(
                "form",
                { action: true },
                output
            )
        );
    }
});
"use strict";
/*! home/home.jsx */
rc.homePageComponent = React.createClass({
    displayName: "homePageComponent",
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        return React.createElement(
            "div",
            { id: "homepage" },
            React.createElement(
                "h1",
                null,
                "Accessibility Tutorial"
            ),
            React.createElement(
                "a",
                { href: "https://bitbucket.org/willbuckingham/accessibility-tutorial" },
                "Link to Readme and Repo"
            )
        );
    }
});
"use strict";
/*! visibleHIdden/visibleHiddenPageComponent.jsx */rc.visibileHiddenPageComponent = React.createClass({
    displayName: "visibileHiddenPageComponent",
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        var output;
        var modifier = [];
        if (app.status.currentFragsArray[0] == "1") {
            output = React.createElement(
                "div",
                null,
                React.createElement(
                    "div",
                    { className: "item", id: "item1" },
                    React.createElement(
                        "h3",
                        null,
                        "Hello"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Ima Link"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item2" },
                    React.createElement(
                        "h3",
                        null,
                        "Hello"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Ima Link"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item3" },
                    React.createElement(
                        "h3",
                        null,
                        "Hello"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Ima Link"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item4" },
                    React.createElement(
                        "h3",
                        null,
                        "Hello"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Ima Link"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item5" },
                    React.createElement(
                        "h3",
                        null,
                        "Hello"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Ima Link"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item6" },
                    React.createElement(
                        "h3",
                        null,
                        "Hello"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Ima Link"
                    )
                )
            );
        } else if (app.status.currentFragsArray[0] == "2") {
            output = React.createElement(
                "div",
                null,
                React.createElement(
                    "div",
                    { className: "item", id: "item1", style: { display: "none" } },
                    React.createElement(
                        "h3",
                        null,
                        "Heading 1"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Link 1"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item2", hidden: true },
                    React.createElement(
                        "h3",
                        null,
                        "Heading 2"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Link 2"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item3", style: { opacity: 0 } },
                    React.createElement(
                        "h3",
                        null,
                        "Heading 3"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Link 3"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item4", style: { visibility: "hidden" } },
                    React.createElement(
                        "h3",
                        null,
                        "Heading 4"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Link 4"
                    )
                ),
                React.createElement(
                    "div",
                    { id: "item5", className: "item \t\tvisuallyhidden" },
                    React.createElement(
                        "h3",
                        null,
                        "Heading 5"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Link 5"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "item", id: "item6", "aria-hidden": "true" },
                    React.createElement(
                        "h3",
                        null,
                        "Heading 6"
                    ),
                    React.createElement(
                        "a",
                        { href: "#imalink" },
                        "Link 6"
                    )
                )
            );
        }
        return React.createElement(
            "div",
            { id: "visibleHidden", className: "container-fluid" },
            output
        );
    }
});
"use strict";
/*! header/header.jsx */
rc.header = React.createClass({
    displayName: "header",
    render: function render() {
        return React.createElement(
            "h2",
            null,
            "Backbone Multipage Boilerplate"
        );
    }
});
'use strict';
/*! loader/loader.jsx */
rc.loader = React.createClass({
    displayName: 'loader',
    stack: [],
    getInitialState: function getInitialState() {
        return {
            show: false
        };
    },
    componentDidMount: function componentDidMount(currentPage) {
        var self = this;
        grandCentral.off('loaderStart').on('loaderStart', function (uniqueString) {
            if ($.inArray(uniqueString, self.stack) == -1) {
                console.log('loaderStart(' + uniqueString + ')');
                self.stack.push(uniqueString);
                self.setState({ show: true });
            }
        });
        grandCentral.off('loaderEnd').on('loaderEnd', function (uniqueString) {
            var i = $.inArray(uniqueString, self.stack);
            if (i > -1) {
                self.stack.splice(i, 1);
                console.log('loaderEnd(' + uniqueString + ')');
            }
            if (self.stack.length === 0) {
                self.setState({ show: false });
            }
        });
    },
    reset: function reset() {
        this.stack = [];
        this.setState({ show: false });
    },
    render: function render() {
        var classes = this.state.show ? 'active' : '';
        return React.createElement(
            'div',
            { id: 'loader', className: classes },
            React.createElement(
                'div',
                { className: 'loadingmessage' },
                React.createElement('img', { className: 'spinner', src: SiteConfig.assetsDirectory + 'images/ui/spinner.gif' })
            )
        );
    }
});
'use strict';
/*! mainmodal/mainmodal.jsx */
rc.mainmodal = React.createClass({
    displayName: 'mainmodal',
    getInitialState: function getInitialState() {
        return {
            show: false,
            whichTemplate: ''
        };
    },
    componentDidMount: function componentDidMount() {
        var self = this;
        grandCentral.off('modalHide').on('modalHide', function () {
            self.setState({ show: false, whichTemplate: '' });
        });
        grandCentral.off('modalShow').on('modalShow', function (payLoad) {
            self.setState({ show: true, whichTemplate: payLoad });
        });
    },
    handleModalClose: function handleModalClose() {
        grandCentral.trigger('modalHide');
        if (app.status.currentFragString) {
            if (app.status.currentFragString.indexOf('modalShow-') > -1) {
                var newURL = '#/' + app.status.currentRoute;
                var stringToRemove = 'modalShow-' + this.state.whichTemplate;
                console.log('removing ' + stringToRemove + 'from the URL');
                newURL = newURL.replace('/' + stringToRemove, '');
                newURL = newURL.replace(stringToRemove + '/', '');
                newURL = newURL.replace(stringToRemove, '');
                app.navigate(newURL);
            }
        }
    },
    render: function render() {
        console.log(this.constructor.displayName + ' render()');
        var self = this;
        var classes = this.state.show ? 'absolutewrapper active' : 'absolutewrapper ';
        var outputArray = [];
        switch (this.state.whichTemplate) {
            case 'attackontitanModal':
                outputArray.push(React.createElement(rc.attackontitanModal, null));break;
            case 'deathnoteModal':
                outputArray.push(React.createElement(rc.deathnoteModal, null));break;
        }
        return React.createElement(
            'div',
            { className: classes },
            React.createElement(
                'div',
                { className: 'greybacking' },
                React.createElement(
                    'div',
                    { className: 'modalwrapper' },
                    React.createElement(
                        'div',
                        { className: 'modalCloseButtonWrapper' },
                        React.createElement(
                            'div',
                            { className: 'modalCloseButton', onClick: self.handleModalClose },
                            React.createElement('img', { src: SiteConfig.assetsDirectory + 'images/ui/modal-close-btn.png' })
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'modalContentsWrapper' },
                        outputArray
                    )
                )
            )
        );
    }
});
'use strict';
/*! nav/nav.jsx */
rc.nav = React.createClass({
	displayName: 'nav',
	getInitialState: function getInitialState() {
		return {
			currentPage: ''
		};
	},
	componentDidMount: function componentDidMount() {
		var self = this;
		grandCentral.off('pagechange').on('pagechange', function (data) {
			self.setState({
				currentPage: data.currentPage
			});
		});
	},
	getClassNameWithActive: function getClassNameWithActive(arg) {
		var className = 'navitem';
		if (arg == this.state.currentPage) {
			className = className + ' active';
		}
		return className;
	},
	render: function render() {
		return React.createElement(
			'div',
			null,
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('home'), href: '#' },
				'Home'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('iconbuttons'), href: '#/iconbuttons/bad' },
				'Accessible Icon Buttons'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('buttonevents'), href: '#/buttonevents/bad' },
				'Accessible Button Events'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('forms'), href: '#/forms/bad' },
				'Building Forms with Accessibility in Mind'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive(''), href: 'http://www.wikipedia.org/en', target: '_blank' },
				'Semantics (see Wikipedia)'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('focusmanagement'), href: '#/focusmanagement/bad' },
				'Focus Management'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('accessibilitytree'), href: '#/accessibilitytree/' },
				'Accessibility Tree'
			),
			React.createElement(
				'a',
				{ className: this.getClassNameWithActive('visibilehidden'), href: '#/visibilehidden/1' },
				'Visible vs. Hidden'
			)
		);
	}
});