# Accessibility Tutorial

## [Egghead.io - Start Building Accessible Web Applications Today](https://egghead.io/courses/start-building-accessible-web-applications-today) (Video Series)

### Accessible Icon Buttons

```html
<button>Help</button>					<!-- GOOD -->

<button>
	<i class="icon icon-help"></i>		<!-- Weak -->
</button>

<div class="button">					<!-- Bad -->
	<svg width="32" height="32" viewBox="0 0 32 32" class="icon">
		<path d="..."></path>
	</svg>
</div>
```

```html
<button>
	<span class="sr-only">Help</span>					<!-- Visible to Screen Readers Only -->
	<i class="icon icon-help" aria-hidden="true"></i>	<!-- Hidden from Screen Readers -->
</button>	

<div class="button" role="button" tabindex="0">			<!-- Preferrably change to actual butotn, but role and tabindex can make it partially accessible -->
	<svg width="32" height="32" viewBox="0 0 32 32" class="icon" aria-labelledby="title">
		<title id="title">Help</title>
		<path d="..."></path>
	</svg>
</div>
```

#### Side Note

There are various ways to hide content from Screen Readers
[Invisible Content Just for Screen Reader Users](http://webaim.org/techniques/css/invisiblecontent/)
Bootstrap includes the simple class of "sr-only"

For Font Awesome and other similar icon packs
[Font Awesome Accessibility](http://fontawesome.io/accessibility/)
If not Interactive: use aria-hidden on `<i>` and optionally a title attribute, and sr-only
For Interactive: Give your wrapping anchor tag an aria-label, while hiding the `<i>`



### Accessible Button Events

Becareful of Hamburger Buttons that are just div's and ::befores and ::afters

Various things can be added to a div to make it behave like a button.  However, using native elements (a or button, etc.) automatically hooks up support for key events.

Buttons are expected to be triggered using the Space key, while links are expected to be triggered using the Enter key.

[Using the Button Role](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_button_role)



### Building Forms with Accessibility in Mind

At the very least, use an implicit label (wrapping text and input together)

You can use explicit labels with for (htmlFor in JSX)

Better grouping of radio buttons to give them more context




### Headings and semantic structure for accessible web pages

Wikipedia does this well [Wikipedia](https://en.wikipedia.org/wiki/Main_Page)

FF Web Developer Toolbar Extention -> Information -> View Document Outline

H1 Rules - Only 1 Per page and Should be the most important piece of information on the page

Cycle thru headings with **CTRL-OPT-CMD-H**

ROTOR **CTRL-OPT-U**

Landmarks provide a way for users to get around their page skipping common repetative sections of pages to get to the content the want to see.  (This is similar to creating skip-links, which to me seem silly, so a better replacement)

[Roles](https://www.w3.org/TR/wai-aria/complete#roles_categorization)

[Using ARIA landmarks to identify regions on the page](https://www.w3.org/WAI/GL/wiki/Using_ARIA_landmarks_to_identify_regions_of_a_page)

Example:

* `role="search"` around `input type="search"`
* `role="navigation"` potentially similar to `<nav>...</nav>` 

**PROTIP**: Use all the semantics and roles as you can, many html5 semantic tags bring roles automatically and do not require you to respecify them.




### Focus management using CSS, HTML, and JavaScript

When manipulating the DOM, make sure to properly maintain the users focus.

[Modal Windows](https://www.smashingmagazine.com/2014/09/making-modal-windows-better-for-everyone/)

*Skip Links (probably not practical for our application) and land marks seem to work better*



### What is the Accessibility Tree?

How the browser exposes accessibility information for screenreaders to use.

[Chrome Accessibility Tree](chrome://accessibility/)

[Accessibility Developer Tools](https://chrome.google.com/webstore/detail/accessibility-developer-t/fpkknkljclfencbdbgkenhalefipecmb?hl=en) - Audits

IE has a better one (Sorry, I do not have a PC here to show you :) )

Developer Tools Experiments

* [chrome://flags/](chrome://flags/)
* Search "Developer Tools experiments"
* Enable And Relaunch Chrome
* Developer Tools Settings -> Experiments -> Accessibility Inspection
* Restart Dev Tools
* In Elements, in the right pane choose the Accessibility Tab



### Intro to ARIA

Now be sure to read up on an individual role you want to use, to make sure you understand the requirements and any gotcha's.

Do NOT use Abstract Roles.

Use semantic elements before using aria roles, and only use the aria roles as needed.

For example there is an aria-checked attribute however using a proper checkbox is a much more proper idea, so do not go reinveting a checkbox and using aria-checked, unless you really need to. (Why? Because you end up having to recreate most of the behavior you get for free.)

aria-disabled is different from disabled

#### Resource Links

* W3 WAI-ARIA
  * [Roles](https://www.w3.org/TR/wai-aria/complete#roles_categorization)
  * [States and Properties](https://www.w3.org/TR/wai-aria/states_and_properties#global_states)
* [MDN - Using ARIA - Roles and Attributes](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques)
* [Karl Grove's Sandbox - ARIA Cheatsheet](http://karlgroves-sandbox.com/CheatSheets/ARIA-Cheatsheet.html) - Simple Cheatsheet
* [WAI-ARIA Cheatsheet](http://lab.abhinayrathore.com/aria-cheatsheet/) - Has links to W3C Spec



### How Visible vs. Hidden Elements Affect Keyboard/Screen Reader Users

* `display:none` = Hidden from everybody
* `hidden` attribute = html5 equivelent of above, most user agents will render this as display:none
* `opacity: 0` = good for animations that you plan on showing, since internal content is still accessible
* `visibility: hidden` = not visible but takes up space
* `visuallyhidden` class = useful for visually hiding content but showing to screen reader (this is similar to bootstrap's sr-only class)
* `aria-hidden="true"` = completely visible to visual people but hidden from screen readers.  useful when you want to keep something on the screen but not have it read to screen readers.  potentially when a modal is covering background content.  However, links are still focusable.



## General Resource Links

#### W3C Links

- [Web Accessibility Initiative (WAI)](http://www.w3.org/WAI/)
  - [Getting Started with Web Accessibility](https://www.w3.org/WAI/gettingstarted/Overview.html)
- [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/intro/wcag.php)
  - [WCAG 2 at a Glance](https://www.w3.org/WAI/WCAG20/glance/Overview.html) or [PDF](https://www.w3.org/WAI/WCAG20/glance/WCAG2-at-a-Glance.pdf)
  - [How to Meet WCAG 2.0 - Quick Reference](https://www.w3.org/WAI/WCAG20/quickref/)
  - [WCAG 2.0 TR](https://www.w3.org/TR/WCAG20/)
- (And Many More… Don't get lost on their site… It's a rabiit hole...)

#### Good Resource Links

* [MDN Accessibility documentation](https://developer.mozilla.org/en-US/docs/Web/Accessibility)
* [Introduction to Web Accessibility with Google](https://webaccessibility.withgoogle.com/course) - Training Course (Text and Videos)
* [Bootstrap Accessibility](http://getbootstrap.com/getting-started/#accessibility)
* [Accessibility Training Session from Sapling Learning](https://www.dropbox.com/s/8m8burrgdux8d0l/AccessibilityTrainingSession.mp4?dl=0) - (Video 1h57m) (Will has not watched)
* [Web Accessibility in Mind](http://webaim.org/intro)
* Screen Readers
  * [Basic Screen Reader Commands](https://www.paciellogroup.com/blog/2015/01/basic-screen-reader-commands-for-accessibility-testing/)
  * [Apple - VoiceOver - Chapter 2. Learning VO Basics](https://www.apple.com/voiceover/info/guide/_1124.html)
  * [NVDA & JAWS Quick Guide](https://docs.google.com/document/d/12yMJUv7g40EjIZ0pMaSk0qRbOKcyKdiE3ZxY0SmhlIw/edit#heading=h.dye3fij55zpz)

#### Really Misc Links

* [Accessible Expanding/Collapsing Sections](http://ncamftp.wgbh.org/sp/tests/expandingSections/expandingSections.html)
* [15 Rules for Making Accessible Links](https://www.sitepoint.com/15-rules-making-accessible-links/)
* [tabindex and Keyboard Focus](http://accessibleculture.org/articles/2010/05/tabindex/)
* [Practical ARIA Examples](http://heydonworks.com/practical_aria_examples/)
* [Infographics](http://cdn2.hubspot.net/hub/153358/file-234472695-pdf/Deque_Accessibility_Infographic_printable_version.pdf)




### less organized and more recent links

So figure this will prob be worth being a channel sooner than later, and this way we can keep most information together....

Here are some of the resources I have:

* [Web Accessibility for Desingers](https://webaim.org/resources/designers/)
* [Contrast Checker](https://webaim.org/resources/contrastchecker/)
* [WCAG 2 Quick Reference](https://www.w3.org/WAI/WCAG21/quickref/)
* [WCAG 2 Checklist](https://webaim.org/standards/wcag/WCAG2Checklist.pdf)
* [THE EVIL SPEC](https://www.w3.org/TR/2008/REC-WCAG20-20081211/)

theres also a wcag 2.1 (few more reqs)

https://www.w3.org/WAI/standards-guidelines/wcag/new-in-21/

So we can in theory pick the level we want to build/test to.

WCAG 2 or 2.1, and levels A to AAA
"The WCAG document does not recommend that Level AAA conformance be required as a general policy for entire sites because it is not possible to satisfy all Level AAA success criteria for some content."

WCAG 2.0 conformance levels
WCAG 2.0 guidelines are categorized into three levels of conformance in order to meet the needs of different groups and different situations: A (lowest), AA (mid range), and AAA (highest). Conformance at higher levels indicates conformance at lower levels. For example, by conforming to AA, a Web page meets both the A and AA conformance levels. Level A sets a minimum level of accessibility and does not achieve broad accessibility for many situations. For this reason, UC recommends AA conformance for all Web-based information.

For Developers:
* https://developers.google.com/web/fundamentals/accessibility/
* https://egghead.io/courses/start-building-accessible-web-applications-today

Understanding WCAG 2.0 Conformance (slightly technical, but comprehensive)
https://www.w3.org/TR/UNDERSTANDING-WCAG20/conformance.html

To help with my issues of videos from our clients not being complaint, we can do this.... https://www.w3.org/TR/2008/REC-WCAG20-20081211/#conformance-partial
though for them to be compliant themselves for their candidates we'd prob have to give them options to upload alternative text, etc
