rc.nav = React.createClass({
	getInitialState:function(){
        return {
        	currentPage : ''
        }
    },
	componentDidMount : function(){
		var self= this;
        // unbind before binding in case component unmounts/remounts, optionally use componentWillUnmount	
	    grandCentral.off('pagechange').on('pagechange', function(data){
			self.setState({
				currentPage: data.currentPage
			});
	    });
	},
	getClassNameWithActive : function(arg){
		var className = 'navitem';
		if (arg == this.state.currentPage) {
			className = className + ' active';
		}
		return className;
	},
    render:function(){
        return (
			<div>
				<a className={this.getClassNameWithActive('home')} href="#">Home</a>
				<a className={this.getClassNameWithActive('iconbuttons')} href="#/iconbuttons/bad">Accessible Icon Buttons</a>
				<a className={this.getClassNameWithActive('buttonevents')} href="#/buttonevents/bad">Accessible Button Events</a>
				<a className={this.getClassNameWithActive('forms')} href="#/forms/bad">Building Forms with Accessibility in Mind</a>
				<a className={this.getClassNameWithActive('')} href="http://www.wikipedia.org/en" target="_blank">Semantics (see Wikipedia)</a>
				<a className={this.getClassNameWithActive('focusmanagement')} href="#/focusmanagement/bad">Focus Management</a>
				<a className={this.getClassNameWithActive('accessibilitytree')} href="#/accessibilitytree/">Accessibility Tree</a>
				<a className={this.getClassNameWithActive('visibilehidden')} href="#/visibilehidden/1">Visible vs. Hidden</a>
			</div>
        );
    }
});
	