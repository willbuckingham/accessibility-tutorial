rc.accessibleFormsPageComponent = React.createClass({
   
    render:function(){
        console.log(this.constructor.displayName+' render()');

        var output;

        switch(app.status.currentFragsArray[0]){
            case "bad":
            	output = (
    			<div>
                    Your Name 
                    <input type="text" />
                    
                    <p>Favorite Animal</p>
                    <div>
                        <input type="radio" name="animals"/> 
                        Dog
                    </div>
                    <div>
                        <input type="radio" name="animals"/> 
                        Cat
                    </div>
                    <div>
                        <input type="radio" name="animals"/> 
                        Bear
                    </div>
                    <br/>
                    
                    <div className="label-wrap">
                        Hometown
                    </div>
                    <div className="input-wrap">
                        <input type="text" />
                    </div>
                </div>
            	);
            break;
            
            case "good":
            	output = (
            	<div>
                	<label>
                        Your Name 
                        <input type="text" />
                    </label>

                    <p>Favorite Animal</p>
                    <div>
                        <label>
                            <input type="radio" name="animals"/> 
                            Dog
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="radio" name="animals"/> 
                            Cat
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="radio" name="animals"/> 
                            Bear
                        </label>
                    </div>
                    <br/>

                    <div className="label-wrap">
                        <label htmlFor="hometown" >
                            Hometown
                        </label>
                    </div>
                    <div className="input-wrap">
                        <input type="text" id="hometown"/>
                    </div>
                </div>
        		);
                break;

            case "best":
                output = (
                <div>
                    <label>
                        Your Name 
                        <input type="text" />
                    </label>
                    <br/><br/>

                    <fieldset>
                        <legend>Favorite Animal</legend>
                        <div>
                            <label>
                                <input type="radio" name="animals"/> 
                                Dog
                            </label>
                        </div>
                        <div>
                            <label>
                                <input type="radio" name="animals"/> 
                                Cat
                            </label>
                        </div>
                        <div>
                            <label>
                                <input type="radio" name="animals"/> 
                                Bear
                            </label>
                        </div>
                    </fieldset>
                    <br/>
                    
                    <div className="label-wrap">
                        <label for="hometown">
                            Hometown
                        </label>
                    </div>
                    <div className="input-wrap">
                        <input type="text" id="hometown" />
                    </div>
                </div>
                );
                break;
    	}

        return (
			<div id="accessibleForms" className="container-fluid">
			    
			    <h2>Building Forms with Accessibility in Mind</h2>

                <form action>
                    {output}
                </form>

			</div>
        );
    }
});