rc.visibileHiddenPageComponent = React.createClass({
   
    render:function(){
        console.log(this.constructor.displayName+' render()');

        var output;
        var modifier = [];

        if(app.status.currentFragsArray[0] == "1"){
        	output = (
        	<div>
        		<div className="item" id="item1">
        			<h3>Hello</h3>
        			<a href="#imalink">Ima Link</a>
        		</div>
        		<div className="item" id="item2">
        			<h3>Hello</h3>
        			<a href="#imalink">Ima Link</a>
        		</div>
        		<div className="item" id="item3">
        			<h3>Hello</h3>
        			<a href="#imalink">Ima Link</a>
        		</div>
        		<div className="item" id="item4">
        			<h3>Hello</h3>
        			<a href="#imalink">Ima Link</a>
        		</div>
        		<div className="item" id="item5">
        			<h3>Hello</h3>
        			<a href="#imalink">Ima Link</a>
        		</div>
        		<div className="item" id="item6">
        			<h3>Hello</h3>
        			<a href="#imalink">Ima Link</a>
        		</div>
        	</div>
        	);

        	
        }else if(app.status.currentFragsArray[0] == "2"){
    		output = (
    		<div>
    			<div className="item" id="item1" 		style={{display:"none"}}>
    				<h3>Heading 1</h3>
    				<a href="#imalink">Link 1</a>
    			</div>
    			<div className="item" id="item2" 		hidden>
    				<h3>Heading 2</h3>
    				<a href="#imalink">Link 2</a>
    			</div>
    			<div className="item" id="item3" 		style={{opacity:0}}>
    				<h3>Heading 3</h3>
    				<a href="#imalink">Link 3</a>
    			</div>
    			<div className="item" id="item4" 		style={{visibility:"hidden"}}>
    				<h3>Heading 4</h3>
    				<a href="#imalink">Link 4</a>
    			</div>
    			<div  id="item5" className="item 		visuallyhidden">
    				<h3>Heading 5</h3>
    				<a href="#imalink">Link 5</a>
    			</div>
    			<div className="item" id="item6" 		aria-hidden="true">
    				<h3>Heading 6</h3>
    				<a href="#imalink">Link 6</a>
    			</div>
    		</div>
    		);
        }

        return (
			<div id="visibleHidden" className="container-fluid">


				{output}

			    {/* (div.item#item$>h3{Heading $}+a[href=#imalink]{Link $})*6 */}
			    

			</div>
        );
    }
});