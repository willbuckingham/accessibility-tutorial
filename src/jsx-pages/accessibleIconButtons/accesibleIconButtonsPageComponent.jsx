rc.accesibleIconButtonsPageComponent = React.createClass({
   
    render:function(){
        console.log(this.constructor.displayName+' render()');

        var output;

        if(app.status.currentFragsArray[0] == "bad"){
        	output = (
				<div>
					<button className="btn">Help</button>

					<button className="btn">
						<i className="fa fa-question-circle"></i>
					</button>

					<div className="btn">
						<svg width="32" height="32" viewBox="0 0 32 32" className="icon">
							<path d="M14 24h4v-4h-4v4zM16 8c-3 0-6 3-6 6h4c0-1 1-2 2-2s2 1
							2 2c0 2-4 2-4 4h4c2-0.688 4-2 5-5s-3-5-6-5zM16 0c-8.844 0-16
							7.156-16 16s7.156 16 16 16 16-7.156 16-16-7.156-16-16-16zM16
							28c-6.625 0-12-5.375-12-12s5.375-12 12-12 12 5.375 12 12-5.375
							12-12 12z"></path>
						</svg>
					</div>
				</div>
        	);
        }else if(app.status.currentFragsArray[0] == "good"){
        	output = (
        		<div>
	        		<button className="btn">Help</button>

					<button className="btn">
						<span className="sr-only">Help</span>
						<i className="fa fa-question-circle" aria-hidden="true"></i>
					</button>	

					<div className="btn" role="button" tabindex="0">
						<svg width="32" height="32" viewBox="0 0 32 32" className="icon" aria-labelledby="title">
							<title id="title">Help</title>
							<path d="M14 24h4v-4h-4v4zM16 8c-3 0-6 3-6 6h4c0-1 1-2 2-2s2 1
							2 2c0 2-4 2-4 4h4c2-0.688 4-2 5-5s-3-5-6-5zM16 0c-8.844 0-16
							7.156-16 16s7.156 16 16 16 16-7.156 16-16-7.156-16-16-16zM16
							28c-6.625 0-12-5.375-12-12s5.375-12 12-12 12 5.375 12 12-5.375
							12-12 12z"></path>
						</svg>
					</div>
				</div>
    		);
    	}

        return (
			<div id="accessibleIconButtons" className="container-fluid">
			    
			    <h2>Accessible Icon Buttons</h2>

			    {output}

			</div>
        );
    }
});