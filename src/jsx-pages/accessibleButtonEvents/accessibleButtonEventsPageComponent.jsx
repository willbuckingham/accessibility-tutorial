rc.accessibleButtonEventsPageComponent = React.createClass({
   
    clickHandler:function(){
        alert("hi");
    },

    render:function(){
        console.log(this.constructor.displayName+' render()');
        
        var output;

        switch(app.status.currentFragsArray[0]){
        	case "bad":
                   /*<div id="menu" className="nav-menu js-navigation-hamburger">
                        <div className="nav-menu__hamburger"></div>
                    </div>*/
        		output = (
                    <div className="btn">
                        <i className="fa fa-bars" />
                    </div>
        		);
        		break;

        	case "good":
        		output = (
                    <div className="btn" role="button" tabIndex="0" aria-label="Menu" onClick={this.clickHandler} onKeyPress={this.clickHandler}>
                        <i className="fa fa-bars" />
                    </div>
        		);
        		break;
        }


        return (
			<div id="accessibleButtonEvents" className="container-fluid">
			    
			    <h2>Accessible Button Events</h2>

                <button className="btn" aria-label="Help" onClick={this.clickHandler}>
                    <i className="fa fa-question-circle" />
                </button>

			    {output}

			</div>
        );
    }
    
});