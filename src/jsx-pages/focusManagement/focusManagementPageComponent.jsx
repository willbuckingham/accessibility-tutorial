rc.focusManagementPageComponent = React.createClass({
   
    clickHandlerBad:function(evt){
        //yes, jquery
        jQuery(evt.target).parent().remove();
    },

    clickHandlerGood:function(evt){
        //yes, jquery
        jQuery(evt.target).parent().remove();
        jQuery("li").find('.btn-delete').first().focus();
    },

    render:function(){
        console.log(this.constructor.displayName+' render()');
        
        var output;

        switch(app.status.currentFragsArray[0]){
        	case "bad":
                  
        		output = (
                    <ul className="list">
                        <li>
                            Item 1 <button className="btn-delete" aria-label="Delete Item 1" onClick={this.clickHandlerBad}> X </button>
                        </li>
                        <li>
                            Item 2 <button className="btn-delete" aria-label="Delete Item 2" onClick={this.clickHandlerBad}> X </button>
                        </li>
                        <li>
                            Item 3 <button className="btn-delete" aria-label="Delete Item 3" onClick={this.clickHandlerBad}> X </button>
                        </li>
                    </ul>
        		);
        		break;

        	case "good":
        		output = (
                    <ul className="list">
                        <li>
                            Item 1 <button className="btn-delete" aria-label="Delete Item 1" onClick={this.clickHandlerGood}> X </button>
                        </li>
                        <li>
                            Item 2 <button className="btn-delete" aria-label="Delete Item 2" onClick={this.clickHandlerGood}> X </button>
                        </li>
                        <li>
                            Item 3 <button className="btn-delete" aria-label="Delete Item 3" onClick={this.clickHandlerGood}> X </button>
                        </li>
                    </ul>
                );
        		break;
        }


        return (
			<div id="accessibleButtonEvents" className="container-fluid">
			    
			    <h2>Focus Management</h2>

                <p><a href="#">A link</a>.</p>
                
			    {output}

			</div>
        );
    }
    
});